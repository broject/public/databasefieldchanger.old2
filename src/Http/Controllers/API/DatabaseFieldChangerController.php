<?php

namespace Brojectcode\DatabaseFieldChanger\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Brojectcode\DatabaseFieldChanger\Exceptions\DatabaseFieldAlreadyChangedException;
use Brojectcode\DatabaseFieldChanger\Exceptions\InputTypeNotFoundException;
use Brojectcode\DatabaseFieldChanger\Exceptions\MissingRequiredParametersException;
use Brojectcode\DatabaseFieldChanger\Exceptions\ModelAttributeRuleCouldNotBeFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Prophecy\Exception\Doubler\ClassNotFoundException;

class DatabaseFieldChangerController extends Controller
{
    private $requiredParameters = [
        'classname',
        'entityId',
        'fieldname',
        'oldValue',
        'newValue',
        'type',
        'disabled',
    ];

    public function executeChanging($model, $fieldname, $newValue)
    {
        $model->$fieldname = $newValue;
        $model->save();

        return $model->fresh()->$fieldname;
    }
    
    public function validateRequiredParameter(Request $request)
    {
        if (!$request->has($this->requiredParameters))
            throw new MissingRequiredParametersException('One or more required parameters are missing', 400);
    }
    
    public function validateParameters($model, $fieldname, $oldValue, $newValue, $type)
    {
        $rule = (!empty($model::$rules[$fieldname])) ? $model::$rules[$fieldname] : false;
        if (!$rule)
            throw new ModelAttributeRuleCouldNotBeFoundException('Rule for the given model attribute could not be found', 400);

        $validator = Validator::make([$fieldname => $newValue], [
            $fieldname => $rule,
        ]);

        if ($validator->fails())
            throw new ValidationException($validator);

        if ($model->$fieldname !== $oldValue)
            throw new DatabaseFieldAlreadyChangedException('The value has already been changed in the meantime', 403);
    }

    public function generateModel($classname, $entityId)
    {
        if (!class_exists($classname, $entityId))
            throw new ClassNotFoundException('Class does not exist', $classname);

        $model = App::make($classname)::find($entityId);

        if (!$model)
            throw new ModelNotFoundException('Model could not be found', 400);

        return $model;
    }

    public function change(Request $request)
    {
        // validate if all required parameters are passed
        try {
            $this->validateRequiredParameter($request);
        } catch (MissingRequiredParametersException $exception) {
            return Response::json([
                'message' => $exception->getMessage(),
                'missingParameters' => array_values(array_diff($this->requiredParameters, array_keys($request->all()))),
            ], $exception->getCode());
        }

        // create model
        try {
            $model = $this->generateModel($request->get('classname'), $request->get('entityId'));
        } catch (ClassNotFoundException $exception) {
            return Response::json([
                'message' => $exception->getMessage(),
                'classname' => $exception->getClassname(),
            ], 400);
        } catch (ModelNotFoundException $exception) {
            return Response::json([
                'message' => $exception->getMessage(),
                'classname' => $request->get('classname'),
                'entityId' => $request->get('entityId'),
            ], $exception->getCode());
        }

        // validate given parameters
        try {
            $this->validateParameters($model, $request->get('fieldname'), $request->get('oldValue'), $request->get('newValue'), $request->get('type'));
        } catch (ModelAttributeRuleCouldNotBeFoundException $exception) {
            return Response::json([
                'message' => $exception->getMessage(),
                'classname' => $request->get('classname'),
                'entityId' => $request->get('entityId'),
                'fieldname' => $request->get('fieldname'),
            ], $exception->getCode());
        } catch (ValidationException $exception) {
            return Response::json([
                'message' => $exception->getMessage(),
                'validation' => $exception->errors(),
            ], $exception->status);
        } catch (DatabaseFieldAlreadyChangedException $exception) {
            return Response::json([
                'message' => $exception->getMessage(),
                'value' => $model->fresh()[$request->get('fieldname')],
            ], $exception->getCode());
        }

        $result = $this->executeChanging($model, $request->get('fieldname'), $request->get('newValue'));

        // return success result
        return Response::json([
            'message' => 'The value has been changed successfully',
            'value' => $result,
        ]);
    }
}
