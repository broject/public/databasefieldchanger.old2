<?php

namespace Brojectcode\DatabaseFieldChanger;

use Illuminate\Support\ServiceProvider;

class DatabaseFieldChangerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->publishFiles();
        $this->loadRoutes();
        $this->loadMigrations();
    }
    
    /**
     * Publish Files
     */
    public function publishFiles()
    {
        $this->publishes([
            __DIR__ . '/../resources/js/' => resource_path('js/components/vendor/brojectcode/databasefieldchanger/'), // vue components
        ], ['brojectcode', 'databasefieldchanger']);
    }

    /**
     * Load Routes
     */
    public function loadRoutes()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/routes.php');
    }

    /**
     * load migrations
     */
    public function loadMigrations()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations/2019_06_25_181301_create_databasefieldchanger_mocked_model_table.php');
    }
}
