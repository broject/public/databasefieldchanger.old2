<?php

namespace Brojectcode\DatabaseFieldChanger\Exceptions;

use Exception;

class ModelAttributeRuleCouldNotBeFoundException extends Exception
{

    /**
     * FieldChangerGenericException constructor.
     * @param string $message
     * @param int $code
     * @param array|null $data
     */
    public function __construct(string $message, int $code)
    {
        parent::__construct($message, $code);
    }
}
