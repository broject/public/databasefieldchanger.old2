<?php

namespace Brojectcode\DatabaseFieldChanger\Exceptions;

use Exception;

class MissingRequiredParametersException extends Exception
{

    /**
     * @var array
     */
    protected $data;

    /**
     * FieldChangerGenericException constructor.
     * @param string $message
     * @param int $code
     * @param array|null $data
     */
    public function __construct(string $message, int $code, array $data = null)
    {
        parent::__construct($message, $code);

        $this->data = $data;
    }
}
