<?php

namespace Brojectcode\DatabaseFieldChanger\Exceptions;

use Exception;

class TypeCouldNotBeFoundException extends Exception
{

    /**
     * FieldChangerGenericException constructor.
     * @param string $message
     * @param int $code
     */
    public function __construct(string $message, int $code)
    {
        parent::__construct($message, $code);
    }
}
