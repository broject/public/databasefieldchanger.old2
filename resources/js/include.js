Vue.component('broject-databasefieldchanger-string', require('./components/types/String.vue').default);
Vue.component('broject-databasefieldchanger-textarea', require('./components/types/Textarea.vue').default);
Vue.component('broject-databasefieldchanger-boolean', require('./components/types/Boolean.vue').default);
Vue.component('broject-databasefieldchanger-select', require('./components/types/Select.vue').default);
Vue.component('broject-databasefieldchanger-date', require('./components/types/Date.vue').default);
Vue.component('broject-databasefieldchanger-time', require('./components/types/Time.vue').default);
Vue.component('broject-databasefieldchanger-datetime', require('./components/types/DateTime.vue').default);
