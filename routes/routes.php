<?php

use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')
    ->prefix('/api/v1/brojectcode/databasefieldchanger/')
    ->namespace('Brojectcode\DatabaseFieldChanger\Http\Controllers\API')
    ->group(__DIR__.'/api.php');
