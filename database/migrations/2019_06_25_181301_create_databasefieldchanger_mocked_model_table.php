<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatabasefieldchangerMockedModelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (env('APP_ENV') === 'testing') {
            Schema::create('brojectcode_databasefieldchanger_mocked_model', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('string')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (env('APP_ENV') === 'testing') {
            Schema::dropIfExists('brojectcode_databasefieldchanger_mocked_model');
        }
    }
}
