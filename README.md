# DatabaseFieldChanger [Laravel + Vue]

You want the ability to save input to your website instantly. Also you dont wan't to struggle with forms in every single project ?
Then this package may help you.

It provides vue components for changing a single field in the database.

## Installation

Install the package through composer:

```bash
composer require brojectcode/databasefieldchanger
```

If you're running Laravel 5.4 or earlier, add the service provider to your config/app.php file:

```php
Brojectcode\DatabaseFieldChanger\DatabaseFieldChangerServiceProvider::class,
```

Publish vue components and edit them if you want:

```bash
php artisan vendor:publish --tag=databasefieldchanger
```

Require them in your app.js file:

```js
require('./components/vendor/brojectcode/databasefieldchanger/include.js');
```

Now you're ready to go !



Brojectcode\Databasefieldchanger
    
## Usage

### Model preparation

In your model, which you want to use the DatabaseFieldChanger on, you have to add rules for your attributes like so.

```php
public $rules = [
    'string' => 'string|nullable',
    'textarea' => 'string|nullable',
    'boolean' => 'boolean',
    'date' => 'date',
    'time' => 'date_format:H:i:s',
    'datetime' => 'date',
    'select' => 'integer|nullable',
];
```

### Component Integration

The following component integrations can now be placed in any blade file you want.

#### String:

```
<broject-databasefieldchanger-string
    classname="{{ \App\User::class }}"
    :entity-id="{{ \App\User::first()->id }}"
    label="Name"
    fieldname="name"
    value="{{ \App\User::first()->name }}"
    :disabled="false"
></broject-databasefieldchanger-string>
```
    
#### Textarea:

```
<broject-databasefieldchanger-textarea
    classname="{{ \App\User::class }}"
    :entity-id="{{ \App\User::first()->id }}"
    label="Name"
    fieldname="name"
    value="{{ \App\User::first()->name }}"
    :disabled="false"
></broject-databasefieldchanger-textarea>
```
    
#### Boolean:
   
``` 
<broject-databasefieldchanger-boolean
    classname="{{ \App\Model::class }}"
    :entity-id="{{ \App\Model::first()->id }}"
    label="Boolean"
    fieldname="boolean"
    :value="@json(\App\Model::first()->boolean)"
    :disabled="false"
></broject-databasefieldchanger-boolean>
```
    
#### Select:

```
<broject-databasefieldchanger-select
    classname="{{ \App\Model::class }}"
    :entity-id="{{ \App\Model::first()->id }}"
    label="Select"
    fieldname="select"
    :items="[
        {name: 'Grafik/Videosignal', value: 1},
        {name: 'Steuersignal', value: 2},
    ]"
    item-text="name"
    item-value="value"
    :value="@json(\App\Model::first()->select)"
    :disabled="false"
    nullable
></broject-databasefieldchanger-select>
```

#### Date:

```
<broject-databasefieldchanger-date
    classname="{{ \App\Model::class }}"
    :entity-id="{{ \App\Model::first()->id }}"
    label="Date"
    fieldname="date"
    value="{{ \App\Model::first()->date }}"
    :disabled="false"
></broject-databasefieldchanger-date>
```
    
#### Time:

```
<broject-databasefieldchanger-time
    classname="{{ \App\Model::class }}"
    :entity-id="{{ \App\Model::first()->id }}"
    label="Time"
    fieldname="time"
    value="{{ \App\Model::first()->time }}"
    :disabled="false"
></broject-databasefieldchanger-time>
```
     
#### DateTime:
    
```
<broject-databasefieldchanger-time
    classname="{{ \App\Model::class }}"
    :entity-id="{{ \App\Model::first()->id }}"
    label="Time"
    fieldname="time"
    value="{{ \App\Model::first()->time }}"
    :disabled="false"
></broject-databasefieldchanger-time>
```

## Questions

### What if a value has been changed in the meantime ?

John and Diana are currently editing the same model.
First John edits the title of the givrn model.
Diana does not reload the page, so she doesn't get the new value.

If Diana tries to change the value, it fails and the value of John will be set to Dianas UI.
> **NOTE** Maybe I'm implementing a pusher automation update for the UI. Let me hear from you if you are interested.

## Tests

The controller to change a value in the database for the given input-type is fully tested via phpunit.
> **NOTE** Dusk / Vue Tests will be coming soon

