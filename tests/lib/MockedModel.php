<?php

namespace Brojectcode\DatabaseFieldChanger\Tests\lib;

use Illuminate\Database\Eloquent\Model;

class MockedModel extends Model
{
    protected $table = 'brojectcode_databasefieldchanger_mocked_model';

    protected $fillable = [
        'string',
    ];

    static public $rules = [
        'string' => 'required|string|min:3|max:255',
    ];
}
