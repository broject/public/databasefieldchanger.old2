<?php

namespace Brojectcode\DatabaseFieldChanger\Tests\lib;

use App\User;
use Illuminate\Foundation\Testing\TestCase;
use Tests\CreatesApplication;

abstract class BaseTestCase extends TestCase
{
    use CreatesApplication;

    protected $user;

    public function signIn($user = null)
    {
        if(! $user){
            $this->user = factory(User::class)->create();
        }

        $this->actingAs($this->user);

        return $this;
    }
}
