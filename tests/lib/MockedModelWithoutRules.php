<?php

namespace Brojectcode\DatabaseFieldChanger\Tests\lib;

use Illuminate\Database\Eloquent\Model;

class MockedModelWithoutRules extends Model
{
    protected $table = 'brojectcode_databasefieldchanger_mocked_model';

    protected $fillable = [
        'string',
    ];
}

