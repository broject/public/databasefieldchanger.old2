<?php

namespace Brojectcode\DatabaseFieldChanger\Tests\api;

use Brojectcode\DatabaseFieldChanger\Tests\lib\BaseTestCase;
use Brojectcode\DatabaseFieldChanger\Tests\lib\MockedModel;
use Brojectcode\DatabaseFieldChanger\Tests\lib\MockedModelWithoutRules;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DatabaseFieldChangerTest extends BaseTestCase
{
    use RefreshDatabase;

    protected $mockedModel;

    public function setUp() : void
    {
        parent::setUp();
        $this->mockedModel = MockedModel::create([
            'string' => 'Mocked Model',
        ]);
    }

    /** @test */
    public function returns_error_if_required_parameters_are_missing()
    {
        $this->signIn();

        $response = $this
            ->withHeader('Accept', 'application/json')
            ->actingAs($this->user, 'api')
            ->json('POST', route('brojectcode-databasefieldchanger.api.change'));

        $response
            ->assertStatus(400)
            ->assertHeader('content-type', 'application/json')
            ->assertExactJson([
                'missingParameters' => [
                    'classname',
                    'entityId',
                    'fieldname',
                    'newValue',
                    'oldValue',
                    'type',
                    'disabled',
                ],
                'message' => 'One or more required parameters are missing'
            ]);
    }

    /** @test */
    public function returns_error_if_models_classname_could_not_be_found()
    {
        $this->signIn();

        $response = $this
            ->withHeader('Accept', 'application/json')
            ->actingAs($this->user, 'api')
            ->json('POST', route('brojectcode-databasefieldchanger.api.change'), [
                'classname' => 'Wrong\\Classname',
                'entityId' => 1,
                'fieldname' => 'string',
                'newValue' => 'new value',
                'oldValue' => 'old value',
                'type' => 'string',
                'disabled' => false,
            ]);

        $response
            ->assertStatus(400)
            ->assertHeader('content-type', 'application/json')
            ->assertExactJson([
                'message' => 'Class does not exist',
                'classname' => 'Wrong\\Classname',
            ]);
    }

    /** @test */
    public function returns_error_if_entity_could_not_be_found()
    {
        $this->signIn();

        $response = $this
            ->withHeader('Accept', 'application/json')
            ->actingAs($this->user, 'api')
            ->json('POST', route('brojectcode-databasefieldchanger.api.change'), [
                'classname' => MockedModel::class,
                'entityId' => -99,
                'fieldname' => 'string',
                'newValue' => 'new value',
                'oldValue' => 'old value',
                'type' => 'string',
                'disabled' => false,
            ]);

        $response
            ->assertStatus(400)
            ->assertHeader('content-type', 'application/json')
            ->assertExactJson([
                'message' => 'Model could not be found',
                'classname' => MockedModel::class,
                'entityId' => -99,
            ]);
    }

    /** @test */
    public function returns_error_if_model_attribute_rule_could_not_be_found()
    {
        $this->signIn();

        $this->mockedModel = MockedModelWithoutRules::create([
            'string' => 'old value',
        ]);

        $response = $this
            ->withHeader('Accept', 'application/json')
            ->actingAs($this->user, 'api')
            ->json('POST', route('brojectcode-databasefieldchanger.api.change'), [
                'classname' => MockedModelWithoutRules::class,
                'entityId' => $this->mockedModel->id,
                'fieldname' => 'string',
                'newValue' => 'new value',
                'oldValue' => 'old value',
                'type' => 'string',
                'disabled' => false,
            ]);

        $response
            ->assertStatus(400)
            ->assertHeader('content-type', 'application/json')
            ->assertExactJson([
                'message' => 'Rule for the given model attribute could not be found',
                'classname' => MockedModelWithoutRules::class,
                'entityId' => $this->mockedModel->id,
                'fieldname' => 'string',
            ]);
    }
    
    /** @test */
    public function returns_error_if_validation_failed()
    {
        $this->signIn();

        $response = $this
            ->withHeader('Accept', 'application/json')
            ->actingAs($this->user, 'api')
            ->json('POST', route('brojectcode-databasefieldchanger.api.change'), [
                'classname' => MockedModel::class,
                'entityId' => $this->mockedModel->id,
                'fieldname' => 'string',
                'newValue' => 'nv', // string to short
                'oldValue' => $this->mockedModel->string,
                'type' => 'string',
                'disabled' => false,
            ]);

        $response
            ->assertStatus(422)
            ->assertHeader('content-type', 'application/json')
            ->assertExactJson([
                'message' => 'The given data was invalid.',
                'validation' => [
                    'string' => ['The string must be at least 3 characters.']
                ],
            ]);
    }

    /** @test */
    public function returns_error_if_field_has_been_changed_in_the_meantime()
    {
        $this->signIn();

        $response = $this
            ->withHeader('Accept', 'application/json')
            ->actingAs($this->user, 'api')
            ->json('POST', route('brojectcode-databasefieldchanger.api.change'), [
                'classname' => MockedModel::class,
                'entityId' => $this->mockedModel->id,
                'fieldname' => 'string',
                'newValue' => 'new value', // string to short
                'oldValue' => 'not the old value',
                'type' => 'string',
                'disabled' => false,
            ]);

        $response
            ->assertStatus(403)
            ->assertHeader('content-type', 'application/json')
            ->assertExactJson([
                'message' => 'The value has already been changed in the meantime',
                'value' => 'Mocked Model',
            ]);
    }

    /** @test */
    public function string_field_could_be_changed_successfully()
    {
        $this->signIn();

        $response = $this
            ->withHeader('Accept', 'application/json')
            ->actingAs($this->user, 'api')
            ->json('POST', route('brojectcode-databasefieldchanger.api.change'), [
                'classname' => MockedModel::class,
                'entityId' => $this->mockedModel->id,
                'fieldname' => 'string',
                'newValue' => 'new value', // string to short
                'oldValue' => $this->mockedModel->string,
                'type' => 'string',
                'disabled' => false,
            ]);

        $response
            ->assertStatus(200)
            ->assertHeader('content-type', 'application/json')
            ->assertExactJson([
                'message' => 'The value has been changed successfully',
                'value' => 'new value',
            ]);
    }


}
